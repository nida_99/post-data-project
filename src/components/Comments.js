import React from "react";
import '../App.css'

class Comments extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            newArr: [],
            showData: false,

        };
    }

    handleClick = (id) => {

        const newArr = this.props.comments.filter((ele) => ele.postId === id)
        this.setState({
            newArr: newArr,
            showData: !this.state.showData,
        })

    }

    render() {
        return (
            <>
                <button onClick={() => this.handleClick(this.props.id)} className="commentsBtn bold">Comments : {this.props.count} </button>
                <div className="comment-container">
                    {

                        this.state.showData ?


                            this.state.newArr.map((ele) => {

                                if (this.props.id === ele.postId) {
                                    return (<div key={ele.id} className="single-comment">
                                        {
                                            <div>
                                                <div className="comment-header">
                                                    <div className="bold">  {ele.name}</div>
                                                    <div className="bold emailContainer">Email {ele.email}</div>
                                                </div>
                                                <p className="para">{ele.body}</p>
                                            </div>
                                        }

                                    </div>)

                                }

                            })

                            :

                            undefined


                    }
                </div>
            </>
        )
    }
}

export default Comments